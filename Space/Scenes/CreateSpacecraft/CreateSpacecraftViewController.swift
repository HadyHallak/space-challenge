//
//  CreateSpacecraftViewController.swift
//  Space
//
//  Created by Hady on 5/7/21.
//

import UIKit
import RxSwift
import RxCocoa

final class CreateSpacecraftViewController: BaseViewController<CreateSpacecraftViewModel> {
    @IBOutlet private weak var totalLabel: UILabel!
    @IBOutlet private weak var textField: UITextField!
    @IBOutlet private weak var durabilitySlider: UISlider!
    @IBOutlet private weak var speedSlider: UISlider!
    @IBOutlet private weak var capacitySlider: UISlider!
    private let continueButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        textField.delegate = self
        navigationItem.rightBarButtonItem = continueButton
        title = "Create Spacecraft"
    }

    override func createViewModel() -> CreateSpacecraftViewModel {
        CreateSpacecraftViewModel(space: space)
    }

    override func bindViewModel() {
        setSlidersBounds()
        bindDurability()
        bindSpeed()
        bindCapacity()
        bindTotalLabel()
        bindTextField()
        bindButton()

        viewModel.created
            .subscribe(onNext: { [unowned self] created in
                if created {
                    self.dismiss(animated: true)
                }
            })
            .disposed(by: disposeBag)
    }

    private func setSlidersBounds() {
        let allSliders: [UISlider] = [durabilitySlider, speedSlider, capacitySlider]
        allSliders.forEach {
            $0.minimumValue = viewModel.min
            $0.maximumValue = viewModel.max
        }
    }

    private func bindDurability() {
        viewModel.durability.bind(to: durabilitySlider.rx.value).disposed(by: disposeBag)
        durabilitySlider
            .rx
            .value
            .map { round($0) }
            .subscribe { [weak self] durability in
                self?.viewModel.setDurability(durability)
            }
            .disposed(by: disposeBag)
    }

    private func bindSpeed() {
        viewModel.speed.bind(to: speedSlider.rx.value).disposed(by: disposeBag)
        speedSlider
            .rx
            .value
            .map { round($0) }
            .subscribe { [weak self] durability in
                self?.viewModel.setSpeed(durability)
            }
            .disposed(by: disposeBag)
    }

    private func bindCapacity() {
        viewModel.capacity.bind(to: capacitySlider.rx.value).disposed(by: disposeBag)
        capacitySlider
            .rx
            .value
            .map { round($0) }
            .subscribe { [weak self] durability in
                self?.viewModel.setCapacity(durability)
            }
            .disposed(by: disposeBag)
    }

    private func bindTotalLabel() {
        viewModel
            .total
            .map { String(format: "Dagitilacak Puan: %d", $0) }
            .bind(to: totalLabel.rx.text)
            .disposed(by: disposeBag)
    }

    private func bindTextField() {
        textField.rx.text.orEmpty.bind(to: viewModel.name).disposed(by: disposeBag)
    }

    private func bindButton() {
        viewModel.isValid.bind(to: continueButton.rx.isEnabled).disposed(by: disposeBag)
        continueButton.rx.tap.bind(to: viewModel.continueTapped).disposed(by: disposeBag)
    }
}

extension CreateSpacecraftViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.textField.resignFirstResponder()
        return true
    }
}
