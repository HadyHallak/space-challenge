//
//  CreateSpacecraftViewModel.swift
//  Space
//
//  Created by Hady on 5/7/21.
//

import RxSwift
import RxRelay
import RxCocoa

final class CreateSpacecraftViewModel: BaseViewModel {
    let min: Float = 1
    let max: Float = 15
    let sliderCount: Float = 3

    let durability: BehaviorRelay<Float>
    let speed: BehaviorRelay<Float>
    let capacity: BehaviorRelay<Float>
    let total: Observable<Int>
    let name: BehaviorRelay<String>
    let isValid: Observable<Bool>
    let continueTapped: PublishRelay<Void>
    private(set) var created: Observable<Bool>!

    override init(space: SpaceManager) {
        durability = BehaviorRelay(value: max / sliderCount)
        speed = BehaviorRelay(value: max / sliderCount)
        capacity = BehaviorRelay(value: max / sliderCount)
        total = Observable.combineLatest(durability, speed, capacity).map { Int($0 + $1 + $2) }
        name = BehaviorRelay(value: "")
        isValid = name.map { !$0.isEmpty }
        continueTapped = PublishRelay<Void>()
        super.init(space: space)

        let spacecraft = Observable.combineLatest(name, durability, speed, capacity) {
            SpaceManager.Spacecraft(name: $0, durability: Int($1), speed: Int($2), capacity: Int($3))
        }
        created = continueTapped.asObservable()
            .withLatestFrom(spacecraft)
            .flatMap { [unowned self] spacecraft in
                self.create(spacecraft: spacecraft)
                    .map { true }
                    .catchAndReturn(false)
            }
    }

    func setDurability(_ newValue: Float) {
        guard newValue + speed.value + capacity.value <= max else {
            durability.accept(durability.value)
            return
        }
        durability.accept(newValue)
    }

    func setSpeed(_ newValue: Float) {
        guard durability.value + newValue + capacity.value <= max else {
            speed.accept(speed.value)
            return
        }

        speed.accept(newValue)
    }

    func setCapacity(_ newValue: Float) {
        guard durability.value + speed.value + newValue <= max else {
            capacity.accept(capacity.value)
            return
        }

        capacity.accept(newValue)
    }

    private func create(spacecraft: SpaceManager.Spacecraft) -> Observable<Void> {
        do {
            try space.createSpacecraft(spacecraft)
            return .just(())
        } catch {
            return .error(error)
        }
    }
}
