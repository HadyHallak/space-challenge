//
//  HomeViewController.swift
//  Space
//
//  Created by Hady on 5/8/21.
//

import UIKit
import RxSwift
import RxCocoa

class HomeViewController: BaseViewController<HomeViewModel> {
    @IBOutlet private weak var ugsLabel: UILabel!
    @IBOutlet private weak var eusLabel: UILabel!
    @IBOutlet private weak var dsLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var healthLabel: UILabel!
    @IBOutlet private weak var countdownLabel: UILabel!
    @IBOutlet private weak var currentSationLabel: UILabel!
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var layout: CarouselFlowLayout!
    @IBOutlet private weak var searchBar: UISearchBar!

    private var favorites: FavoriteService!

    convenience init(space: SpaceManager, favorites: FavoriteService) {
        self.init(nibName: nil, bundle: nil)
        self.space = space
        self.favorites = favorites
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Station"
        collectionView.register(UINib(nibName: "StationCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "StationCollectionViewCell")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.viewWillAppear()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        layout.itemSize = collectionView.frame.insetBy(dx: 30, dy: 30).size
    }

    override func createViewModel() -> HomeViewModel {
        HomeViewModel(space: space)
    }

    override func bindViewModel() {
        viewModel.spacecraftName.bind(to: nameLabel.rx.text).disposed(by: disposeBag)
        viewModel.ugs.bind(to: ugsLabel.rx.text).disposed(by: disposeBag)
        viewModel.eus.bind(to: eusLabel.rx.text).disposed(by: disposeBag)
        viewModel.ds.bind(to: dsLabel.rx.text).disposed(by: disposeBag)
        viewModel.countdown.bind(to: countdownLabel.rx.text).disposed(by: disposeBag)
        viewModel.health.bind(to: healthLabel.rx.text).disposed(by: disposeBag)
        viewModel.currentStation.bind(to: currentSationLabel.rx.text).disposed(by: disposeBag)
        viewModel
            .stations
            .bind(to: collectionView.rx.items(cellIdentifier: "StationCollectionViewCell", cellType: StationCollectionViewCell.self))
            { [unowned space, unowned favorites] index, station, cell in
                let viewModel = StationCellViewModel(space: space!, favorites: favorites!, station: station)
                cell.bind(viewModel: viewModel)
            }
            .disposed(by: disposeBag)

        searchBar.rx.text
            .orEmpty
            .distinctUntilChanged()
            .debounce(RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [unowned self] text in
                self.viewModel.setSearchTerm(text)
            })
            .disposed(by: self.disposeBag)

        searchBar.rx.searchButtonClicked
            .subscribe { [unowned self] _ in
                self.searchBar.resignFirstResponder()
            }
            .disposed(by: disposeBag)
    }
}
