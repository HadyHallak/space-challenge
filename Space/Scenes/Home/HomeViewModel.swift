//
//  HomeViewModel.swift
//  Space
//
//  Created by Hady on 5/8/21.
//

import Foundation
import RxSwift
import RxRelay
import RxCocoa

final class HomeViewModel: BaseViewModel {
    let spacecraft = PublishRelay<SpaceManager.Spacecraft?>()
    let stations = BehaviorRelay<[SpaceManager.Station]>(value: [])
    let spacecraftName: Observable<String?>
    let countdown: Observable<String>
    let health: Observable<String>
    let ugs: Observable<String>
    let eus: Observable<String>
    let ds: Observable<String>
    let currentStation: Observable<String?>
    private var searchTerm: String = ""

    override init(space: SpaceManager) {
        spacecraftName = spacecraft.map { $0?.name }
        countdown = spacecraft.map { $0?.countdown ?? 0 }.map { String(format: "%ds", $0) }
        health = spacecraft.map { $0?.damage ?? 0 }.map { 100 - $0 }.map { String($0) }
        ugs = spacecraft.map { $0?.UGS ?? 0 }.map { String(format: "UGS: %d", $0) }
        eus = spacecraft.map { $0?.EUS ?? 0 }.map { String(format: "EUS: %d", $0) }
        ds = spacecraft.map { $0?.DS ?? 0 }.map { String(format: "DS: %d", $0) }
        currentStation = spacecraft.map { $0?.station }
        super.init(space: space)
        updateStations()
        space.addObserver(self)
    }

    func setSearchTerm(_ term: String) {
        searchTerm = term.trimmingCharacters(in: .whitespacesAndNewlines)
        updateStations()
    }

    func viewWillAppear() {
        updateStations()
    }

    private func updateStations() {
        guard !searchTerm.isEmpty else {
            stations.accept(space.storage.stations)
            return
        }

        let filtered = space.storage.stations.filter { $0.name.lowercased().contains(searchTerm.lowercased()) }
        stations.accept(filtered)
    }

    deinit {
        space.removeObserver(self)
    }
}

extension HomeViewModel: SpaceObserver {
    func spaceManager(_ spaceManager: SpaceManager, spacecraftUpdated spacecraft: SpaceManager.Spacecraft?) {
        self.spacecraft.accept(spacecraft)
    }

    func spaceManager(_ spaceManager: SpaceManager, stationsUpdated stations: [SpaceManager.Station]) {
        updateStations()
    }
}
