//
//  StationCollectionViewCell.swift
//  Space
//
//  Created by Hady on 5/9/21.
//

import UIKit
import RxSwift

class StationCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var capacityStockLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var travelButton: UIButton!

    var viewModel: StationCellViewModel!
    private var disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
        clipsToBounds = true
        layer.cornerRadius = 20
    }

    func bind(viewModel: StationCellViewModel) {
        // Clear any previously binded streams
        disposeBag = DisposeBag()

        self.viewModel = viewModel
        viewModel.capacityStock.bind(to: capacityStockLabel.rx.text).disposed(by: disposeBag)
        viewModel.distance.bind(to: distanceLabel.rx.text).disposed(by: disposeBag)
        viewModel.name.bind(to: nameLabel.rx.text).disposed(by: disposeBag)
        viewModel.isFavorite.bind(to: favoriteButton.rx.isSelected).disposed(by: disposeBag)
        viewModel.canTravel.bind(to: travelButton.rx.isEnabled).disposed(by: disposeBag)
        viewModel.unableToTravelReason.bind(to: travelButton.rx.title(for: .disabled)).disposed(by: disposeBag)

        favoriteButton.rx.tap
            .subscribe { [weak viewModel] _ in
                viewModel?.favoriteTapped()
            }
            .disposed(by: disposeBag)

        travelButton.rx.tap
            .subscribe { [weak viewModel] _ in
                viewModel?.travelTapped()
            }
            .disposed(by: disposeBag)
    }
}
