//
//  StationCellViewModel.swift
//  Space
//
//  Created by Hady on 5/9/21.
//

import Foundation
import RxSwift
import RxRelay

final class StationCellViewModel: BaseViewModel {
    private let favorites: FavoriteService
    let station: SpaceManager.Station
    let capacityStock: Observable<String>
    let distance: Observable<String>
    let name: Observable<String>
    let isFavorite: BehaviorRelay<Bool>
    let canTravel: BehaviorRelay<Bool>
    let unableToTravelReason: ReplayRelay<String>

    init(space: SpaceManager, favorites: FavoriteService, station: SpaceManager.Station) {
        self.favorites = favorites
        self.station = station
        capacityStock = .just(String(format: "%d/%d", station.capacity, station.stock))
        distance = .just(String(format: "%dEUS", space.distance(to: station)))
        name = .just(station.name)
        isFavorite = BehaviorRelay(value: favorites.isFavorite(station))

        do {
            _ = try space.canTravel(to: station).get()
            canTravel = BehaviorRelay(value: true)
            unableToTravelReason = ReplayRelay.create(bufferSize: 1)
        } catch {
            canTravel = BehaviorRelay(value: false)
            unableToTravelReason = ReplayRelay.create(bufferSize: 1)
            unableToTravelReason.accept(error.localizedDescription)
        }

        super.init(space: space)
    }

    func favoriteTapped() {
        if favorites.isFavorite(station) {
            favorites.remove(station)
            isFavorite.accept(false)
        } else {
            favorites.add(station)
            isFavorite.accept(true)
        }
    }

    func travelTapped() {
        try? space.travel(to: station)
    }
}
