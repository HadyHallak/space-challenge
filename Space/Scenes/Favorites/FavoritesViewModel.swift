//
//  FavoritesViewModel.swift
//  Space
//
//  Created by Hady on 5/8/21.
//

import Foundation
import RxSwift
import RxRelay

final class FavoritesViewModel: BaseViewModel {
    private let favorites: FavoriteService
    let stations = BehaviorRelay<[SpaceManager.Station]>(value: [])

    init(space: SpaceManager, favorites: FavoriteService) {
        self.favorites = favorites
        super.init(space: space)

        updateStations()
        space.addObserver(self)
    }

    func viewAppeared() {
        updateStations()
    }

    func removeStationAt(_ index: Int) {
        favorites.remove(stations.value[index])
        updateStations()
    }

    private func updateStations() {
        let favorites = favorites.filterAndReturnFavoritesOnly(space.storage.stations)
        self.stations.accept(favorites)
    }
}

extension FavoritesViewModel: SpaceObserver {
    func spaceManager(_ spaceManager: SpaceManager, stationsUpdated stations: [SpaceManager.Station]) {
        updateStations()
    }
}
