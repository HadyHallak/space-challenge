//
//  FavoritesViewController.swift
//  Space
//
//  Created by Hady on 5/8/21.
//

import UIKit

class FavoritesViewController: BaseViewController<FavoritesViewModel> {
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var emptyMessageLabel: UILabel!

    private var favorites: FavoriteService!

    convenience init(space: SpaceManager, favorites: FavoriteService) {
        self.init(nibName: nil, bundle: nil)
        self.space = space
        self.favorites = favorites
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Favorites"
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.tableFooterView = UIView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.viewAppeared()
    }

    override func createViewModel() -> FavoritesViewModel {
        FavoritesViewModel(space: space, favorites: favorites)
    }

    override func bindViewModel() {
        viewModel.stations
            .bind(to: tableView.rx.items(cellIdentifier: "cell")) { index, station, cell in
                cell.textLabel?.text = station.name
                if #available(iOS 13.0, *) {
                    cell.accessoryView = UIImageView(image: UIImage(systemName: "star.fill"))
                }
            }
            .disposed(by: disposeBag)

        viewModel.stations
            .map { !$0.isEmpty }
            .bind(to: emptyMessageLabel.rx.isHidden)
            .disposed(by: disposeBag)

        tableView.rx.dataSource
            .methodInvoked(#selector(UITableViewDataSource.tableView(_:commit:forRowAt:)))
            .filter { $0[1] as? Int == UITableViewCell.EditingStyle.delete.rawValue }
            .map { ($0.first as! UITableView, $0.last as! IndexPath) }
            .subscribe(onNext: { [weak viewModel] tableView, indexPath in
                viewModel?.removeStationAt(indexPath.row)
            })
            .disposed(by: disposeBag)
    }
}
