//
//  TabBarController.swift
//  Space
//
//  Created by Hady on 5/8/21.
//

import UIKit
import RxSwift

final class TabBarController: UITabBarController {
    private var space: SpaceManager!
    private var favorites: FavoriteService!
    private var viewModel: TabBarViewModel!
    private let disposeBag = DisposeBag()

    convenience init(space: SpaceManager, favorites: FavoriteService) {
        self.init()
        self.space = space
        self.favorites = favorites
        viewModel = TabBarViewModel(space: space)
        bindViewModel()
        setupViewControllers()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.viewDidAppear()
    }

    private func bindViewModel() {
        viewModel
            .needsToCreateSpacecraft
            .subscribe(onNext: { [weak self] _ in self?.presentCreateSpacecraft() })
            .disposed(by: disposeBag)
    }

    private func presentCreateSpacecraft() {
        let viewController = UINavigationController(rootViewController: CreateSpacecraftViewController(space: space))
        viewController.modalPresentationStyle = .fullScreen
        present(viewController, animated: false)
    }

    private func setupViewControllers() {
        let home = HomeViewController(space: space, favorites: favorites)
        let favorites =  UINavigationController(rootViewController: FavoritesViewController(space: space, favorites: favorites))
        favorites.title = "Favorites"
        viewControllers = [
            home,
            favorites
        ]

        if #available(iOS 13.0, *) {
            home.tabBarItem.image = UIImage(systemName: "house")
            home.tabBarItem.selectedImage = UIImage(systemName: "house.fill")
            favorites.tabBarItem.image = UIImage(systemName: "star")
            favorites.tabBarItem.selectedImage = UIImage(systemName: "star.fill")
        }
    }
}
