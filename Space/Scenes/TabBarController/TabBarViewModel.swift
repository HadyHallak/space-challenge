//
//  TabBarViewModel.swift
//  Space
//
//  Created by Hady on 5/9/21.
//

import Foundation
import RxSwift
import RxRelay

final class TabBarViewModel: BaseViewModel {
    let needsToCreateSpacecraft = PublishRelay<Void>()

    override init(space: SpaceManager) {
        super.init(space: space)
        // TODO: listen for app foreground/background notifications and pause resume timer..
    }

    func viewDidAppear() {
        if !space.storage.hasSpacecraft {
            needsToCreateSpacecraft.accept(())
            WebAPI.fetchStations { [weak self] in
                self?.handleStationsFetch($0)
            }
        } else {
            try? space.startFloatingInSpace()
        }
    }

    private func handleStationsFetch(_ result: Result<[WebAPI.Station], WebAPI.Error>) {
        switch result {
        case let .success(stations):
            space.setStations(stations.map(self.mapToSpaceManagerStation(_:)))
        case let .failure(error):
            print("Failed to fetch stations with error", error)
        }
    }

    private func mapToSpaceManagerStation(_ station: WebAPI.Station) -> SpaceManager.Station {
        SpaceManager.Station(
            name: station.name,
            coordinateX: station.coordinateX,
            coordinateY: station.coordinateY,
            capacity: station.capacity,
            stock: station.stock,
            need: station.need
        )
    }
}
