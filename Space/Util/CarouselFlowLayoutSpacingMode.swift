//
//  CarouselFlowLayoutSpacingMode.swift
//  Space
//
//  Created by Hady on 5/9/21.
//

enum CarouselFlowLayoutSpacingMode {
    case fixed
    case overlap
}
