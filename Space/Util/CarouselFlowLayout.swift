//
//  CarouselFlowLayout.swift
//  Space
//
//  Created by Hady on 5/9/21.
//

import UIKit

/// A configurable carousel collection view layout that provides options for item scale and spacing.
class CarouselFlowLayout: UICollectionViewFlowLayout {
    var sideItemScale: CGFloat = 0.8
    var sideItemAlpha: CGFloat = 1
    var fixedSpacing: CGFloat = 10
    var spacingVisibleOffset: CGFloat = 0
    var spacingMode = CarouselFlowLayoutSpacingMode.fixed

    private var actualMinimumLineSpacing: CGFloat = 0

    override open func prepare() {
        super.prepare()
        self.setupCollectionView()
        self.updateLayout()
    }

    open func setupCollectionView() {
        guard let collectionView = self.collectionView else { return }
        if collectionView.decelerationRate != UIScrollView.DecelerationRate.fast {
            collectionView.decelerationRate = UIScrollView.DecelerationRate.fast
        }
    }

    open func updateLayout() {
        guard let collectionView = self.collectionView else { return }

        let collectionSize = collectionView.bounds.size
        let isHorizontal = (self.scrollDirection == .horizontal)

        let yInset = (collectionSize.height - self.itemSize.height) / 2
        let xInset = (collectionSize.width - self.itemSize.width) / 2

        if isHorizontal {
            self.sectionInset = UIEdgeInsets.init(top: yInset, left: xInset, bottom: yInset, right: xInset)
        } else {
            self.sectionInset = UIEdgeInsets.init(top: yInset, left: xInset, bottom: yInset, right: xInset)
        }

        let side = isHorizontal ? self.itemSize.width : self.itemSize.height
        let scaledItemOffset =  (side - side*self.sideItemScale) / 2
        switch self.spacingMode {
        case .fixed:
            actualMinimumLineSpacing = fixedSpacing - scaledItemOffset
            minimumLineSpacing = max(0, actualMinimumLineSpacing)
        case .overlap:
            let fullSizeSideItemOverlap = spacingVisibleOffset + scaledItemOffset
            let inset = isHorizontal ? xInset : yInset
            actualMinimumLineSpacing = inset - fullSizeSideItemOverlap
            minimumLineSpacing = max(0, inset - fullSizeSideItemOverlap)
        }

        minimumInteritemSpacing = 0
    }

    override open func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }

    override open func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard
            let superAttributes = super.layoutAttributesForElements(in: rect),
            let attributes = NSArray(array: superAttributes, copyItems: true) as? [UICollectionViewLayoutAttributes]
            else { return nil }
        return attributes.map { self.transformLayoutAttributes($0) }
    }

    open override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        if let attributes = super.layoutAttributesForItem(at: indexPath) {
            return transformLayoutAttributes(attributes)
        }

        return nil
    }

    private func transformLayoutAttributes(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        guard let collectionView = self.collectionView else { return layoutAttributes }

        let isHorizontal = (scrollDirection == .horizontal)
        let collectionCenter = isHorizontal ? collectionView.frame.size.width/2 : collectionView.frame.size.height/2
        let offset = isHorizontal ? collectionView.contentOffset.x : collectionView.contentOffset.y
        let normalizedCenter = (isHorizontal ? layoutAttributes.center.x : layoutAttributes.center.y) - offset

        let maxDistance = (isHorizontal ? itemSize.width : itemSize.height) + actualMinimumLineSpacing
        let distance = min(abs(collectionCenter - normalizedCenter), maxDistance)
        let ratio = (maxDistance - distance)/maxDistance

        return applyTransform(on: layoutAttributes, centerRatio: ratio)
    }

    open func applyTransform(on layoutAttributes: UICollectionViewLayoutAttributes, centerRatio ratio: CGFloat) -> UICollectionViewLayoutAttributes {
        let alpha = ratio * (1 - self.sideItemAlpha) + self.sideItemAlpha
        let scale = ratio * (1 - self.sideItemScale) + self.sideItemScale
        layoutAttributes.alpha = alpha
        layoutAttributes.transform3D = CATransform3DScale(CATransform3DIdentity, scale, scale, 1)
        layoutAttributes.zIndex = Int(alpha * 10)

        return layoutAttributes
    }

    override open func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        guard let collectionView = collectionView else { return .zero }

        let parent = super.targetContentOffset(forProposedContentOffset: proposedContentOffset, withScrollingVelocity: velocity)

        let itemSpace = itemSize.width + minimumLineSpacing
        var currentItemIdx = collectionView.contentOffset.x / itemSpace

        // Skip to the next cell, if there is residual scrolling velocity left.
        // This helps to prevent glitches
        let vX = velocity.x
        if vX > 0 {
            currentItemIdx += 1
        } else if vX < 0 {
            currentItemIdx -= 1
        }

        let nearestPageOffset = currentItemIdx.rounded() * itemSpace

        return CGPoint(x: nearestPageOffset, y: parent.y)
    }
}
