//
//  AppDelegate.swift
//  Space
//
//  Created by Hady on 5/7/21.
//

import UIKit

@main
final class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        createWindow()
        return true
    }

    private func createWindow() {
        window = UIWindow()
        window?.rootViewController = TabBarController(space: createSpaceManager(), favorites: createFavoriteService())
        window?.makeKeyAndVisible()
    }

    private func createSpaceManager() -> SpaceManager {
        SpaceManager(storage: SpaceUserDefaultsStorage())
    }

    private func createFavoriteService() -> FavoriteService {
        FavoriteService()
    }
}

