//
//  WebAPI.swift
//  Space
//
//  Created by Hady on 5/8/21.
//

import Foundation

enum WebAPI {
    static func fetchStations(completion: @escaping (Result<[Station], WebAPI.Error>) -> Void) {
        let url = URL(string: "https://run.mocky.io/v3/e7211664-cbb6-4357-9c9d-f12bf8bab2e2")!
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                completion(.failure(.networkError(error: error)))
                return
            }

            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                completion(.failure(WebAPI.Error.serverError))
                return
            }
            if let data = data {
                do {
                    let stations = try JSONDecoder().decode([Station].self, from: data)
                    completion(.success(stations))
                } catch {
                    completion(.failure(.decodeError(error: error)))
                }
            } else {
                completion(.success([]))
            }
        }
        task.resume()
    }
}

extension WebAPI {
    struct Station: Codable {
        let name: String
        let coordinateX: Double
        let coordinateY: Double
        let capacity: Int
        let stock: Int
        let need: Int
    }
}

extension WebAPI {
    enum Error: Swift.Error {
        case networkError(error: Swift.Error)
        case serverError
        case decodeError(error: Swift.Error)
    }
}
