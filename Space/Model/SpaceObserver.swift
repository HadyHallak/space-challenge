//
//  SpaceObserver.swift
//  Space
//
//  Created by Hady on 5/9/21.
//

import Foundation

protocol SpaceObserver: AnyObject {
    func spaceManager(_ spaceManager: SpaceManager, spacecraftUpdated spacecraft: SpaceManager.Spacecraft?)
    func spaceManager(_ spaceManager: SpaceManager, stationsUpdated stations: [SpaceManager.Station])
}

extension SpaceObserver {
    func spaceManager(_ spaceManager: SpaceManager, spacecraftUpdated spacecraft: SpaceManager.Spacecraft?) {}
    func spaceManager(_ spaceManager: SpaceManager, stationsUpdated stations: [SpaceManager.Station]) {}
}
