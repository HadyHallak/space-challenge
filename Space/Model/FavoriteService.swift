//
//  FavoriteService.swift
//  Space
//
//  Created by Hady on 5/9/21.
//

import Foundation

final class FavoriteService {
    private let key = "favorites"
    private let defaults: UserDefaults

    init(defaults: UserDefaults = .standard) {
        self.defaults = defaults
    }

    private(set) var all: [String] {
        get {
            UserDefaults.standard.stringArray(forKey: key) ?? []
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: key)
        }
    }

    func add(_ station: SpaceManager.Station) {
        all.append(station.name)
    }

    func remove(_ station: SpaceManager.Station) {
        all.removeAll { $0 == station.name }
    }

    func isFavorite(_ station: SpaceManager.Station) -> Bool {
        return all.contains(station.name)
    }

    func filterAndReturnFavoritesOnly(_ stations: [SpaceManager.Station]) -> [SpaceManager.Station] {
        stations.filter { all.contains($0.name) }
    }
}
