//
//  SpaceUserDefaultsStorage.swift
//  Space
//
//  Created by Hady on 5/8/21.
//

import Foundation

final class SpaceUserDefaultsStorage: SpaceStorage {
    var spacecraft: SpaceManager.Spacecraft?
    var stations: [SpaceManager.Station] = []
    private let defaults: UserDefaults

    init(defaults: UserDefaults = .standard) {
        self.defaults = defaults
        spacecraft = get(codable: SpaceManager.Spacecraft.self,
                         forKey: Keys.spacecraft.rawValue,
                         defaults: defaults)
        stations = get(codable: [SpaceManager.Station].self,
                       forKey: Keys.stations.rawValue,
                       defaults: defaults) ?? []
    }

    var hasSpacecraft: Bool {
        return spacecraft != nil
    }

    func setSpacecraft(_ spacecraft: SpaceManager.Spacecraft?) {
        self.spacecraft = spacecraft
        set(codable: spacecraft, forKey: Keys.spacecraft.rawValue, defaults: defaults)
    }

    func setStations(_ stations: [SpaceManager.Station]) {
        self.stations = stations
        set(codable: stations, forKey: Keys.stations.rawValue, defaults: defaults)
    }

    func updateStation(_ station: SpaceManager.Station) {
        var stations = self.stations
        if let index = stations.firstIndex(where: { $0.name == station.name }) {
            stations.remove(at: index)
            stations.insert(station, at: index)
            setStations(stations)
        }
    }
}

private extension SpaceUserDefaultsStorage {
    func set<T: Codable>(codable value: T?, forKey key: String, defaults: UserDefaults) {
        let data = try? JSONEncoder().encode(value)
        defaults.setValue(data, forKey: key)
    }

    func get<T: Codable>(codable: T.Type, forKey key: String, defaults: UserDefaults) -> T? {
        if let data = defaults.value(forKey: key) as? Data {
            let parsed = try? JSONDecoder().decode(T.self, from: data)
            return parsed
        }

        return nil
    }
}

private enum Keys: String {
    case spacecraft
    case stations
}
