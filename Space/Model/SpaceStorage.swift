//
//  SpaceStorage.swift
//  Space
//
//  Created by Hady on 5/8/21.
//

protocol SpaceStorage {
    var hasSpacecraft: Bool { get }
    var spacecraft: SpaceManager.Spacecraft? { get }
    var stations: [SpaceManager.Station] { get }

    func setSpacecraft(_ spacecraft: SpaceManager.Spacecraft?)
    func setStations(_ stations: [SpaceManager.Station])
    func updateStation(_ station: SpaceManager.Station)
}
