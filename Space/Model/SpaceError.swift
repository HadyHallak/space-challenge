//
//  SpaceError.swift
//  Space
//
//  Created by Hady on 5/8/21.
//

import Foundation

enum SpaceError: LocalizedError {
    case invalidSpacecraftInfo
    case noSpacecraft
    case alreadyInSpace
    case insufficientTime
    case currentlyAtStation
    case stationIsFull

    var errorDescription: String? {
        switch self {
        case .insufficientTime:
            return "Insufficent time"
        case .currentlyAtStation:
            return "Currently at station"
        case .stationIsFull:
            return "Station is full"
        default:
            return nil
        }
    }
}

