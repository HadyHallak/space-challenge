//
//  SpaceManager.swift
//  Space
//
//  Created by Hady on 5/8/21.
//

import Foundation

final class SpaceManager {
    init(storage: SpaceStorage) {
        self.storage = storage
    }

    let storage: SpaceStorage
    private lazy var observers = MulticastDelegate<SpaceObserver>()
    private var damageTimer: Timer?

    func createSpacecraft(_ spacecraft: Spacecraft) throws {
        if !isValid(spacecraft: spacecraft) {
            throw SpaceError.invalidSpacecraftInfo
        }

        var spacecraft = spacecraft
        spacecraft.UGS = spacecraft.capacity * 10_000
        spacecraft.EUS = spacecraft.speed * 20
        spacecraft.DS = spacecraft.durability * 10_000
        spacecraft.station = "Dunya"
        storage.setSpacecraft(spacecraft)
        invokeObservers {
            $0.spaceManager(self, spacecraftUpdated: spacecraft)
        }
    }

    func setStations(_ stations: [Station]) {
        storage.setStations(stations)
        invokeObservers {
            $0.spaceManager(self, stationsUpdated: stations)
        }
    }

    func startFloatingInSpace() throws {
        guard storage.spacecraft != nil else {
            throw SpaceError.noSpacecraft
        }

        guard damageTimer == nil || damageTimer?.isValid == false else {
            throw SpaceError.alreadyInSpace
        }

        setupDamageTimer()
    }

    private func setupDamageTimer() {
        var spacecraft = storage.spacecraft!

        if shouldReturnToEarth(spacecraft: spacecraft) {
            returnToEarth(notifyObservers: true)
            return
        }

        // Use saved countdown if present, otherwise start a new countdown (DS / 1000)
        if spacecraft.countdown == nil || spacecraft.countdown! <= 0 {
            spacecraft.countdown = spacecraft.DS / 1000
            storage.setSpacecraft(spacecraft)
        }
        invokeObservers {
            $0.spaceManager(self, spacecraftUpdated: spacecraft)
        }

        damageTimer?.invalidate()
        damageTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] timer in
            self?.handleCountdownTick(timer)
        }
    }

    private func handleCountdownTick(_ timer: Timer) {
        guard var spacecraft = storage.spacecraft else {
            timer.invalidate()
            return
        }

        if let countdown = spacecraft.countdown, countdown > 0 {
            spacecraft.countdown = countdown - 1
            let isFinished = spacecraft.countdown == 0
            if isFinished {
                spacecraft.damage += 10
            }
            storage.setSpacecraft(spacecraft)
            invokeObservers {
                $0.spaceManager(self, spacecraftUpdated: spacecraft)
            }
            if isFinished {
                timer.invalidate()
                setupDamageTimer()
            }
        } else {
            timer.invalidate()
        }
    }

    func distance(to station: Station) -> Int {
        let coordinateX = storage.spacecraft?.coordinateX ?? 0
        let coordinateY = storage.spacecraft?.coordinateY ?? 0

        let from = (x: coordinateX, y: coordinateY)
        let to = (x: station.coordinateX, y: station.coordinateY)

        let distanceSquared = (from.x - to.x) * (from.x - to.x) + (from.y - to.y) * (from.y - to.y)

        return Int(sqrt(distanceSquared))
    }

    func canTravel(to station: Station) -> Result<Spacecraft, SpaceError> {
        guard let spacecraft = storage.spacecraft else {
            return .failure(.noSpacecraft)
        }

        if spacecraft.coordinateX == station.coordinateX,
           spacecraft.coordinateY == station.coordinateY {
            return .failure(.currentlyAtStation)
        }

        if station.need <= 0 {
            return .failure(.stationIsFull)
        }

        let distance = self.distance(to: station)
        if distance > spacecraft.EUS {
            return .failure(.insufficientTime)
        }

        return .success(spacecraft)
    }

    func travel(to station: Station) throws {
        var spacecraft = try canTravel(to: station).get()

        let sentUGS = min(spacecraft.UGS, station.need)
        spacecraft.EUS -= distance(to: station)
        spacecraft.UGS -= sentUGS
        spacecraft.station = station.name
        spacecraft.coordinateX = station.coordinateX
        spacecraft.coordinateY = station.coordinateY
        storage.setSpacecraft(spacecraft)

        var station = station
        station.stock += sentUGS
        station.need -= sentUGS
        storage.updateStation(station)

        if shouldReturnToEarth(spacecraft: spacecraft) {
            damageTimer?.invalidate()
            returnToEarth(notifyObservers: false)
        }

        invokeObservers {
            $0.spaceManager(self, spacecraftUpdated: spacecraft)
            $0.spaceManager(self, stationsUpdated: storage.stations)
        }
    }

    func addObserver(_ observer: SpaceObserver) {
        observers.addDelegate(observer)
    }

    func removeObserver(_ observer: SpaceObserver) {
        observers.removeDelegate(observer)
    }

    private func shouldReturnToEarth(spacecraft: Spacecraft) -> Bool {
        if spacecraft.damage == 100 || spacecraft.UGS == 0 || spacecraft.EUS == 0 {
            return true
        }

        if spacecraft.coordinateX == 0, spacecraft.coordinateY == 0 {
            return false
        }

        let stationsWithNeeds = storage.stations.filter { $0.need > 0 }
        if stationsWithNeeds.count == 0 {
            return true
        }

        for station in stationsWithNeeds {
            let distance = self.distance(to: station)
            if distance <= spacecraft.EUS {
                return false
            }
        }

        return true
    }

    private func returnToEarth(notifyObservers: Bool = true) {
        guard var spacecraft = storage.spacecraft else { return }
        spacecraft.UGS = spacecraft.capacity * 10_000
        spacecraft.EUS = spacecraft.speed * 20
        spacecraft.DS = spacecraft.durability * 10_000
        spacecraft.damage = 0
        spacecraft.station = "Dunya"
        spacecraft.coordinateX = 0
        spacecraft.coordinateY = 0
        storage.setSpacecraft(spacecraft)

        if notifyObservers {
            invokeObservers {
                $0.spaceManager(self, spacecraftUpdated: spacecraft)
            }
        }

        try! startFloatingInSpace()
    }

    private func isValid(spacecraft: Spacecraft) -> Bool {
        let total = spacecraft.durability + spacecraft.speed + spacecraft.capacity
        let trimmedNamed = spacecraft.name.trimmingCharacters(in: .whitespacesAndNewlines)
        return total <= 15 && spacecraft.durability > 0 && spacecraft.speed > 0 && spacecraft.capacity > 0 && !trimmedNamed.isEmpty
    }

    private func invokeObservers(_ invocation: (SpaceObserver) -> Void) {
        observers |> invocation
    }
}

