//
//  SpaceManager+Models.swift
//  Space
//
//  Created by Hady on 5/9/21.
//

import Foundation

extension SpaceManager {
    struct Spacecraft: Codable {
        let name: String
        let durability: Int
        let speed: Int
        let capacity: Int
        var damage = 0
        var station: String?
        var coordinateX = 0.0
        var coordinateY = 0.0
        var UGS = 0
        var EUS = 0
        var DS = 0
        var countdown: Int?
    }

    struct Station: Codable {
        let name: String
        let coordinateX: Double
        let coordinateY: Double
        let capacity: Int
        var stock: Int
        var need: Int
    }
}
