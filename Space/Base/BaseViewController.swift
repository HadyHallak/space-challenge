//
//  BaseViewController.swift
//  Space
//
//  Created by Hady on 5/8/21.
//

import UIKit
import RxSwift

class BaseViewController<ViewModel>: UIViewController {
    var space: SpaceManager!
    private(set) var viewModel: ViewModel!
    private(set) lazy var disposeBag = DisposeBag()

    convenience init(space: SpaceManager) {
        self.init(nibName: nil, bundle: nil)
        self.space = space
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = createViewModel()
        bindViewModel()
    }

    func createViewModel() -> ViewModel {
        fatalError("Override in subclasses")
    }

    func bindViewModel() {

    }
}
