//
//  BaseViewModel.swift
//  Space
//
//  Created by Hady on 5/8/21.
//

import Foundation

class BaseViewModel {
    let space: SpaceManager

    init(space: SpaceManager) {
        self.space = space
    }
}
